package com.eugene.controller;

import com.eugene.entity.Pizza;
import com.eugene.service.PizzaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/pizza")
public class PizzaController {

    @Autowired
    private PizzaService pizzaService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Pizza> getAllPizzas(){
        return pizzaService.getAllPizzas();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Pizza getPizzaById(@PathVariable("id") int id){
        return pizzaService.getPizzaById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void updatePizzaById(@PathVariable("id") int id){
        pizzaService.removePizzaById(id);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updatePizzaById(@RequestBody Pizza pizza){
        pizzaService.updatePizza(pizza);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void insertPizza(@RequestBody Pizza pizza){
        pizzaService.insertPizza(pizza);
    }
}
