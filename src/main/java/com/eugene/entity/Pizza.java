package com.eugene.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pizza {

    private int id;
    private String name;
    private String Taste;


    //Lombok does it all
//    public Pizza(int id, String name, String Taste) {
//        this.id = id;
//        this.name = name;
//        this.Taste = Taste;
//    }
//
//    //public Pizza(){}
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getTaste() {
//        return Taste;
//    }
//
//    public void setTaste(String taste) {
//        this.Taste = taste;
//    }
}
