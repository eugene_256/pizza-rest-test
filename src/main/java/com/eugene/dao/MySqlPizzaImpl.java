package com.eugene.dao;

import com.eugene.entity.Pizza;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

@Repository("mysql")
public class MySqlPizzaImpl implements PizzaDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    class PizzaRowMapper implements RowMapper<Pizza> {
        @Override
        public Pizza mapRow(ResultSet resultSet, int i) throws SQLException {
            Pizza pizza = new Pizza();
            pizza.setId(resultSet.getInt("id"));
            pizza.setName(resultSet.getString("name"));
            pizza.setTaste(resultSet.getString("taste"));
            return pizza;
        }
    }

    @Override
    public Collection<Pizza> getAllPizzas() {
        return jdbcTemplate.query("SELECT id, name, taste FROM pizzas", new PizzaRowMapper());
    }

    @Override
    public Pizza getPizzaById(int id) {
        return jdbcTemplate.queryForObject("SELECT id, name, taste FROM pizzas WHERE id = ?",new PizzaRowMapper(), id);
    }

    @Override
    public void removePizzaById(int id) {
        jdbcTemplate.update("DELETE FROM pizzas WHERE id = ?", id);
    }

    @Override
    public void updatePizza(Pizza pizza) {
        int id = pizza.getId();
        String name = pizza.getName();
        String taste = pizza.getTaste();
        jdbcTemplate.update("UPDATE pizzas SET name = ?, taste = ? WHERE id = ?",new Object[]{name, taste, id});
    }

    @Override
    public void insertPizzaToDb(Pizza pizza) {
        String name = pizza.getName();
        String taste = pizza.getTaste();
        jdbcTemplate.update("INSERT INTO pizzas (name, taste) VALUES( ?, ?)",new Object[]{name, taste});

    }
}
