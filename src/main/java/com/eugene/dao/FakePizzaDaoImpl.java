package com.eugene.dao;

import com.eugene.entity.Pizza;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
@Qualifier("fakeData")
public class FakePizzaDaoImpl implements PizzaDao {

    private static Map<Integer, Pizza> pizzas;

    static {

        pizzas = new HashMap<Integer, Pizza>(){

            {
                put(1, new Pizza(1, "NY", "Cheesy"));
                put(2, new Pizza(2, "Boston", "Many meat"));
                put(3, new Pizza(3, "Poltava", "Village style"));
            }
        };
    }

    @Override
    public Collection<Pizza> getAllPizzas(){
        return this.pizzas.values();
    }

    @Override
    public Pizza getPizzaById(int id){
        return this.pizzas.get(id);
    }

    @Override
    public void removePizzaById(int id) {
        this.pizzas.remove(id);
    }

    @Override
    public void updatePizza(Pizza pizza){
        Pizza s = pizzas.get(pizza.getId());
        s.setTaste(pizza.getTaste());
        s.setName(pizza.getName());
        pizzas.put(pizza.getId(), pizza);
    }

    @Override
    public void insertPizzaToDb(Pizza pizza) {
        this.pizzas.put(pizza.getId(), pizza);
    }
}
