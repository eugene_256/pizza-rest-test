package com.eugene.dao;

import com.eugene.entity.Pizza;

import java.util.Collection;

public interface PizzaDao {
    Collection<Pizza> getAllPizzas();

    Pizza getPizzaById(int id);

    void removePizzaById(int id);

    void updatePizza(Pizza pizza);

    void insertPizzaToDb(Pizza pizza);
}
