package com.eugene.dao;

import com.eugene.entity.Pizza;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;

@Repository
@Qualifier("mongoData")
public class MongoPizzaDaoImpl implements PizzaDao {


    @Override
    public Collection<Pizza> getAllPizzas() {
        return new ArrayList<Pizza>(){
            {
                add(new Pizza(1, "Mexican", "Spicy"));
            }
        };
    }

    @Override
    public Pizza getPizzaById(int id) {
        return null;
    }

    @Override
    public void removePizzaById(int id) {

    }

    @Override
    public void updatePizza(Pizza pizza) {

    }

    @Override
    public void insertPizzaToDb(Pizza pizza) {

    }
}
