package com.eugene.service;

import com.eugene.dao.PizzaDao;
import com.eugene.entity.Pizza;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class PizzaService {

    @Autowired
    @Qualifier("fakeData")
    private PizzaDao pizzaDao;

    public Collection<Pizza> getAllPizzas(){
        return this.pizzaDao.getAllPizzas();
    }

    public Pizza getPizzaById(int id){
        return this.pizzaDao.getPizzaById(id);
    }


    public void removePizzaById(int id) {
        this.pizzaDao.removePizzaById(id);
    }

    public void updatePizza(Pizza pizza){
        this.pizzaDao.updatePizza(pizza);
    }

    public void insertPizza(Pizza pizza) {
        this.pizzaDao.insertPizzaToDb(pizza);
    }
}
