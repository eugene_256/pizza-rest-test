package com.eugene;

import com.eugene.Main;
import com.eugene.entity.Pizza;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

////Конфигурация 1. Работает если поднять сначала сервер
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest

//Конфигурация 2. Со стартом сервера
@RunWith(SpringRunner.class)
@TestPropertySource("classpath:test.properties")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)


public class PizzaControllerIT {

    TestRestTemplate restTemplate = new TestRestTemplate();

    @Value( "${server.port}" )
    private String server_port;

    @Test
    public void testListOfPizzas(){

        ResponseEntity<Collection<Pizza>> responseEntity =
                restTemplate.exchange("http://localhost:" + server_port + "/pizza", HttpMethod.GET, null,
                                        new ParameterizedTypeReference<Collection<Pizza>>() {});

        Collection<Pizza> actualList = responseEntity.getBody();

        //Hamcrest
        assertThat(actualList.size(), is(3));

        //Field presence check
//        List<Integer> actualIds = actualList.stream()
//                .map(pizza -> pizza.getId())
//                .collect(toList());

        //immutable list with Guava ImmutableList::copyOf
        List<Integer> actualIds = actualList.stream()
                .map(pizza -> pizza.getId())
                .collect(collectingAndThen(toList(), Collections::unmodifiableList));

        assertThat(actualIds, containsInAnyOrder(1, 2, 3));

    }
}