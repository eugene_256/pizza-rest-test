package com.eugene;

import com.eugene.entity.Pizza;
import com.eugene.service.PizzaService;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.eugene.controller.PizzaController;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PizzaControllerTest {

    @Mock
    private PizzaService pizzaService;
    @InjectMocks
    PizzaController pizzaController;

    @Test
    public void testListOfPizzas(){
        //Мокито+Гуава, при вызове возвращать пустой лист
        when(pizzaController.getAllPizzas()).thenReturn(ImmutableList.of());

        List<Pizza> pizzas = (List<Pizza>) pizzaController.getAllPizzas();

        //Проверка, что метод был вызван
        verify(pizzaService).getAllPizzas();

    }
}
